import serial
import math
def calcSpeed(lat1,lon1,lat2,lon2):

    phi1 = lat1 * math.pi/180
    phi2 = lat2 * math.pi/180
    dphi = (lat2-lat1) * math.pi/180
    dlam = (lon2-lon1) * math.pi/180
    a = math.sin(dphi/2) * math.sin(dphi/2) + math.cos(phi1) * math.cos(phi2) * math.sin(dlam/2) * math.sin(dlam/2);
    c = 2 * math.atan2(math.sqrt(a),math.sqrt(1-a))
    d = 6371e3 * c
    print("Speed:",math.floor(d),"m/s Approx.")
    return math.floor(d)
    
def getCoords():
    TSpeed=0
    serialPort = serial.Serial(port = "COM5", baudrate=115200, bytesize=8, timeout=1, stopbits=serial.STOPBITS_ONE) #initializes the Serial Port

    serialPort.open()                           # Opens the Serial Port

    serialString = ""                           # Used to hold data coming over UART

    Points= []                                  #Empty List to store all Points

    prev = 0
    
    while(True):

        # Wait until there is data waiting in the serial buffer
        if(serialPort.in_waiting > 0):

            # Read data out of the buffer 
            serialString = serialPort.readline()

            # Print the contents of the serial data
            data = serialString.decode('Ascii')

            if data == prev:
                break
            else:
                for i in data:
                    indices = [i for i, x in enumerate(data) if x == ","]
                    
                lat=data[indices[2]+1:indices[3]]       #Latitude from Serial GPS Data
                lon=data[indices[4]+1:indices[5]]       #Longitude from Serial GPS Data

                point = [lat,lon]

                if Points!=[]:
                    currSpeed = calcSpeed(Points[-1][0],Points[-1][1],lat,lon)      #Calculate Current Speed
                    print("Current Speed:",currSpeed,"m/s")

                Points.append[point]                    #Adds Points to list
            prev = data
    
    for i in range(len(Points)-1):
        TSpeed+=calcSpeed(Points[i][0],Points[i][1],Points[i+1][0],Points[i+1][1])      #Sum of Speeds
        
    avgSpeed=TSpeed/(len(Points))                                                       #Average Speed over the Whole Journey

    print("Average Speed: ",avgSpeed,"m/s")

getCoords()